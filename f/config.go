package f

import (
	"os"

	"github.com/pelletier/go-toml/v2"
)

var (
	Cfg      *config
	Name     = "MoxySpice"
	Version  = "v0.1.1"
	Home, _  = os.UserHomeDir()
	Config   = Home + "/.moxyspice.toml"
	template = []byte(`#App Config TOML format https://toml.io
# Application must be restarted for changes to take effect
# @pve does not need to be appended to the username.
username="spicy" #UPDATE ME
password=""

# TokenAuth will bypass the login prompt
TokenID=""
Secret=""

# Parameter definitions: https://www.mankier.com/1/remote-viewer
# Will override matching spice file parameters
[RemoteViewer]
fullscreen=1 #enable fullscreen

# Clusters are defined as a list of nodes
[Connections.PVE]
pve="10.0.0.2:8006" #UPDATE ME

#[Connections.Other_cluster]
#oc0="172.16.0.1:8006"
#oc1="172.16.0.2:8006"
#oc2="172.16.0.3:8006"
`)
)

type config struct {
	Username     string
	Password     string
	TokenID      string
	Secret       string
	RemoteViewer map[string]any
	Connections  map[string]map[string]string
	TokenAuth    bool
}

// Write Config writes the default configuration to the configuration file.
func WriteConfig() error {
	if _, err := os.Stat(Config); os.IsNotExist(err) {
		if err := os.WriteFile(Config, template, 0660); err != nil {
			return err
		}
	}
	return nil
}

// ReadConfig reads the configuration file and unmarshals it into the Cfg variable.
func ReadConfig() error {
	file, err := os.ReadFile(Config)
	if err != nil {
		return err
	}
	Cfg = new(config)
	if err := toml.Unmarshal(file, Cfg); err != nil {
		return err
	}
	if Cfg.TokenID != "" && Cfg.Secret != "" {
		Cfg.TokenAuth = true
	}
	return nil
}
