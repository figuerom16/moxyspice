package f

import (
	"context"
	"crypto/tls"
	"fmt"
	"math/rand"
	"net/http"
	"os"
	"os/exec"
	"strings"
	"time"

	"fyne.io/fyne/v2/widget"
	"github.com/luthermonson/go-proxmox"
)

var (
	Message       = widget.NewLabel("")
	OpenCmd       = ""
	ViewerCmd     = ""
	ShowSpice     = false
	Cluster       = ""
	c             = context.Background()
	client        *proxmox.Client
	vms           = []virtualMachine{}
	txtFileName   = Home + "/.spice.txt"
	spiceFileName = Home + "/.spice.vv"
	r             = rand.New(rand.NewSource(time.Now().UnixNano()))
)

type virtualMachine struct {
	Agent  int
	Host   string
	Name   string
	Node   string
	Status string
	Spice  int
	Type   string
	VMID   uint64
}

func SetClient(username string, password string, otp string) {
	var option proxmox.Option
	if Cfg.TokenAuth {
		option = proxmox.WithAPIToken(Cfg.TokenID, Cfg.Secret)
	} else {
		option = proxmox.WithCredentials(&proxmox.Credentials{Username: username + "@pve", Password: password, Otp: otp})
	}
	keys := make([]string, 0, len(Cfg.Connections[Cluster]))
	for k := range Cfg.Connections[Cluster] {
		keys = append(keys, k)
	}
	host := Cfg.Connections[Cluster][keys[r.Intn(len(keys))]]
	url := fmt.Sprintf("https://%s/api2/json", host)
	client = proxmox.NewClient(url, option)
	if _, err := client.Version(c); err != nil {
		insecureHTTPClient := http.Client{
			Transport: &http.Transport{
				TLSClientConfig: &tls.Config{InsecureSkipVerify: true},
			},
		}
		client = proxmox.NewClient(url, option, proxmox.WithHTTPClient(&insecureHTTPClient))
		if _, err := client.Version(c); err != nil {
			Message.Importance = widget.DangerImportance
			Message.SetText("Error: " + err.Error())
			return
		}
		Message.Importance = widget.WarningImportance
		Message.SetText("Warning: Insecure Connection")
	} else {
		Message.Importance = widget.SuccessImportance
		Message.SetText("Success: Secure Connection")
	}
}

func GetVms() []string {
	list := []string{}
	if err := client.Get(c, "/cluster/resources?type=vm", &vms); err != nil {
		Message.Importance = widget.DangerImportance
		Message.SetText("Error: " + err.Error())
		return list
	}
	n := 0
	for _, vm := range vms {
		if vm.Type != "qemu" {
			continue
		}
		if err := client.Get(c, fmt.Sprintf("/nodes/%s/qemu/%d/status/current", vm.Node, vm.VMID), &vm); err != nil {
			continue
		}
		vm.Host = Cfg.Connections[Cluster][vm.Node]
		list = append(list, fmt.Sprintf("%d:%s:%s status:%s agent:%v spice:%v", vm.VMID, vm.Name, vm.Node, vm.Status, vm.Agent == 1, vm.Spice == 1))
		vms[n] = vm
		n++
	}
	vms = vms[:n]
	return list
}

func SpiceProxy(vmid uint64) {
	vm := new(virtualMachine)
	for _, v := range vms {
		if v.VMID == vmid {
			vm = &v
			break
		}
	}
	if err := client.Get(c, fmt.Sprintf("/nodes/%s/qemu/%d/status/current", vm.Node, vm.VMID), vm); err != nil {
		Message.Importance = widget.DangerImportance
		Message.SetText("Error: " + err.Error())
		return
	}
	if vm.Status != "running" {

		if err := client.Post(c, fmt.Sprintf("/nodes/%s/qemu/%d/status/start", vm.Node, vm.VMID), nil, nil); err != nil {
			Message.Importance = widget.DangerImportance
			Message.SetText("Error: " + err.Error())
			return
		}
	}
	sp := make(map[string]any)
	for range 10 {
		if err := client.Post(c, fmt.Sprintf("/nodes/%s/qemu/%d/spiceproxy", vm.Node, vm.VMID), map[string]string{"proxy": strings.Split(vm.Host, ":")[0]}, &sp); err != nil {
			Message.Importance = widget.WarningImportance
			Message.SetText("Warning: " + err.Error())
			time.Sleep(time.Second)
			continue
		}
		break
	}
	if sp == nil {
		Message.Importance = widget.DangerImportance
		Message.SetText("Error: spice proxy failed")
		return
	}
	for k, v := range Cfg.RemoteViewer {
		sp[k] = v
	}
	file, err := os.Create(txtFileName)
	if err != nil {
		Message.Importance = widget.DangerImportance
		Message.SetText("Error: " + err.Error())
		return
	}
	if _, err := file.WriteString("[virt-viewer]\n"); err != nil {
		Message.Importance = widget.DangerImportance
		Message.SetText("Error: " + err.Error())
		return
	}
	for k, v := range sp {
		if _, err := file.WriteString(fmt.Sprintf("%s=%v\n", k, v)); err != nil {
			Message.Importance = widget.DangerImportance
			Message.SetText("Error: " + err.Error())
			return
		}
	}
	if err := file.Close(); err != nil {
		Message.Importance = widget.DangerImportance
		Message.SetText("Error: " + err.Error())
		return
	}
	if ShowSpice {
		if err := exec.Command(OpenCmd, txtFileName).Run(); err != nil {
			Message.Importance = widget.DangerImportance
			Message.SetText("Error: " + err.Error())
		}
		time.Sleep(time.Millisecond * 100)
	}
	if err := os.Rename(txtFileName, spiceFileName); err != nil {
		Message.Importance = widget.DangerImportance
		Message.SetText("Error: " + err.Error())
		return
	}
	go func() {
		if err := exec.Command(ViewerCmd, spiceFileName).Run(); err != nil {
			Message.Importance = widget.DangerImportance
			Message.SetText("Error: " + err.Error())
		}
	}()
	Message.Importance = widget.SuccessImportance
	Message.SetText("Success: Remote Viewer Opened")
}
