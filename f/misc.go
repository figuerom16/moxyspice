package f

import (
	"image/color"
	"os"

	"fyne.io/fyne/v2"
	"fyne.io/fyne/v2/canvas"
)

func PanicMessage(w fyne.Window, err error) {
	PanicMessage := canvas.NewText(err.Error(), &color.RGBA{255, 0, 0, 255})
	PanicMessage.TextStyle = fyne.TextStyle{Bold: true}
	PanicMessage.TextSize = 24
	w.SetContent(PanicMessage)
	w.ShowAndRun()
	os.Exit(1)
}

// LongestString returns the longest string in a slice of strings.
func LongestString(strings []string) string {
	longest := ""
	for _, str := range strings {
		if len(str) > len(longest) {
			longest = str
		}
	}
	return longest
}
