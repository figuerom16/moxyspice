#!/bin/bash

fyne-cross linux -arch=amd64,arm64
fyne-cross windows -arch=amd64,arm64
fyne-cross darwin -arch=amd64,arm64 --macosx-sdk-path /home/matt/Documents/go/MACSDKs/MacOSX11.3.sdk
