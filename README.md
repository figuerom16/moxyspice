# MoxySpice
MoxySpice is a Proxmox spice helper client written in Go and a rewrite of [PVE-VDIClient](https://github.com/joshpatten/PVE-VDIClient). Note that this is a work in progress so PVE-VDIClient is more sophisticated and this is untested. It utilizes Go Proxmox to interact with Proxmox and Fyne for the GUI. Images at the bottom.

### AMD64
- LINUX - https://gitlab.com/figuerom16/bin/-/raw/main/linux-amd64/moxyspice.tar.xz
- WINDOWS - https://gitlab.com/figuerom16/bin/-/raw/main/windows-amd64/moxyspice.exe.zip
- MACOS - https://gitlab.com/figuerom16/bin/-/raw/main/darwin-amd64/moxyspice.app.tar.xz

### ARM64
- LINUX - https://gitlab.com/figuerom16/bin/-/raw/main/linux-arm64/moxyspice.tar.xz
- WINDOWS - https://gitlab.com/figuerom16/bin/-/raw/main/windows-arm64/moxyspice.exe.zip
- MACOS - https://gitlab.com/figuerom16/bin/-/raw/main/darwin-arm64/moxyspice.app.tar.xz

## Getting Started
- For Windows: virt-viewer is required https://virt-manager.org/download.html
- Download the latest release for your OS.
- Extract the archive.
- Run moxyspice (For Linux there is a makefile for installation).
- The first time MoxySpice is run it will generate the necessary Config.
- Edit the ~/.moxyspice.toml. There is a shortcut to the file in the app.
- Restart MoxySpice.

## Setting up the pve user and VM
- For this example we will be creating a "SpiceUser" Role, "moxy" group and "spicy" user.
- In Proxmox navigate to Datacenter -> Permissions
- Click Roles -> Create -> (Name: SpiceUser, Privileges: VM.Audit, VM.Console, VM.PowerMgmt) -> OK
- Click Groups -> Create -> (Name: moxy) -> Create
- Click Users -> Add (User name: spicy, Realm: Proxmox VE authentication server, Add in password, Group: moxy) -> Add
- Do note that API Tokens and Two Factor is here as well.
- In Proxmox navigate to your Datacenter -> (node) -> (VM) -> Permissions -> Add -> ("moxy" group or "spicy" User, Role: SpiceUser) -> Add
- On the same VM navigate to Hardware -> Display -> Edit -> (Graphic card: SPICE, Memory: 32) -> OK
32MB is more than enough memory for 4k so this can be lowered.

## Logging in
- If all the previous steps have been completed properly you will be able to log in with MoxySpice.
- If you are still having issue be sure that the .moxyspice.toml file has node:host:port mapped correctly example (pve:10.1.1.1:8006).

## FAQ
### Q:	Why make MoxySpice?
A:	This was a good way to learn [Go Proxmox](https://github.com/luthermonson/go-proxmox) and since [PVE-VDIClient](https://github.com/joshpatten/PVE-VDIClient) already did all the heavy lifting of figuring out the implementation it seemed like fun to convert their Python to Go. Also cross compiling is easier. My last motive is to eventually make a Go/HTMX/Surreal/BulmaCSS/SQLite server that will act like VMWare Horizon Server. Doing all the stuff such as provisioning clones, handing out machines, blocking other user Spice connections, rolling back snapshots on disconnect, etc, etc. Still dreaming of how to implement this, but wanting to avoid having to install Agents on the VMs. With MoxySpice notifying the Server that it's alive we might able to allow the server to provision/destroy/create clones.

### Q:	Why is Windows Defender flagging MoxySpice?
A:	MoxySpice uses the [Fyne Cross Compiler](https://github.com/fyne-io/fyne-cross) for all builds. Windows Defender flags the binary as a Trojan because the binary is not signed.
Code Signing Certs cost money, but one day I might try the Windows App Store to sign the binary for free.

### Q:	I want to build this project myself?
A:	Clone the project and follow the Fyne guide here: https://developer.fyne.io/started/

### Q:	Why is MoxySpice blinding me?
A:	Fyne uses your system's theme. Set your OS theme to dark mode to save your eyes.

## Special Thanks to the Developers of:
Go Proxmox - https://github.com/luthermonson/go-proxmox  
Fyne GUI - https://github.com/fyne-io/fyne  
Fyne Cross Compiler - https://github.com/fyne-io/fyne-cross  
Go-toml - https://github.com/pelletier/go-toml  
PVE-VDIClient - https://github.com/joshpatten/PVE-VDIClient

And of course the Go Standard Library.

![login](https://gitlab.com/figuerom16/moxyspice/-/raw/main/screenshots/login.png)
![list](https://gitlab.com/figuerom16/moxyspice/-/raw/main/screenshots/list.png)