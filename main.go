package main

import (
	"errors"
	"os"
	"os/exec"
	"runtime"
	"strconv"
	"strings"

	"fyne.io/fyne/v2"
	"fyne.io/fyne/v2/app"
	"fyne.io/fyne/v2/container"
	"fyne.io/fyne/v2/data/binding"
	"fyne.io/fyne/v2/theme"
	"fyne.io/fyne/v2/widget"
	"gitlab.com/figuerom16/moxyspice/f"
)

func main() {
	// Init app and window
	a := app.New()
	w := a.NewWindow(f.Name + " " + f.Version)
	w.SetMaster()

	// Check OS
	switch runtime.GOOS {
	case "linux", "darwin":
		f.OpenCmd = "open"
		output, _ := exec.Command("which", "remote-viewer").Output()
		if len(output) == 0 {
			f.PanicMessage(w, errors.New("panic: remote-viewer not found"))
		}
		f.ViewerCmd = "remote-viewer"
	case "windows":
		f.OpenCmd = "notepad"
		output, _ := exec.Command("where", "/R", os.Getenv("ProgramFiles"), "remote-viewer.exe").Output()
		if len(output) == 0 {
			f.PanicMessage(w, errors.New("panic: remote-viewer not found"))
		}
		f.ViewerCmd = strings.ReplaceAll(strings.TrimSpace(string(output)), "\\", "/")
	default:
		f.PanicMessage(w, errors.New("panic: unsupported os"))
	}

	// Config
	if err := f.WriteConfig(); err != nil {
		f.PanicMessage(w, err)
	}
	if err := f.ReadConfig(); err != nil {
		f.PanicMessage(w, err)
	}

	// Containers
	var login *fyne.Container
	var selector *fyne.Container
	vms := []string{}

	// List
	data := binding.BindStringList(&vms)
	list := widget.NewListWithData(data,
		func() fyne.CanvasObject { return widget.NewLabel(f.LongestString(vms)) },
		func(i binding.DataItem, o fyne.CanvasObject) {
			l := o.(*widget.Label)
			if strings.Contains(l.Text, "spice:false") {
				l.Importance = widget.DangerImportance
			} else if strings.Contains(l.Text, "status:running") {
				l.Importance = widget.HighImportance
			} else {
				l.Importance = widget.MediumImportance
			}
			l.Bind(i.(binding.String))
		},
	)
	list.OnSelected = func(id widget.ListItemID) {
		vmid, err := strconv.ParseUint(strings.Split(vms[id], ":")[0], 10, 64)
		if err != nil {
			f.Message.Importance = widget.DangerImportance
			f.Message.SetText("Error: " + err.Error())
			return
		}
		f.SpiceProxy(vmid)
		list.UnselectAll()
	}

	// Top
	settings := widget.NewButtonWithIcon("Config", theme.FileIcon(), func() {
		if err := exec.Command(f.OpenCmd, f.Config).Run(); err != nil {
			f.Message.Importance = widget.DangerImportance
			f.Message.SetText("Error: " + err.Error())
		}
	})
	f.Message.SetText("Proxmox VE Authentication Server Login")
	f.Message.TextStyle = fyne.TextStyle{Bold: true}
	f.Message.Importance = widget.HighImportance

	// Bottom
	logout := widget.NewButtonWithIcon("Logout", theme.LogoutIcon(), func() {
		f.Message.Importance = widget.SuccessImportance
		f.Message.SetText("Success: Logged Out")
		if f.Cfg.TokenAuth {
			a.Quit()
			return
		}
		w.SetContent(login)
	})
	refresh := widget.NewButtonWithIcon("Refresh", theme.ViewRefreshIcon(), func() {
		f.Message.Importance = widget.SuccessImportance
		f.Message.SetText("Success: Refreshed")
		vms = f.GetVms()
		data.Set(vms)
	})

	// Toggle
	showspice := widget.NewCheck("Show Spice", func(b bool) { f.ShowSpice = b })

	// Groups
	keys := make([]string, 0, len(f.Cfg.Connections))
	for k := range f.Cfg.Connections {
		keys = append(keys, k)
	}
	groups := widget.NewSelect(keys, func(s string) { f.Cluster = s })
	groups.Selected, f.Cluster = keys[0], keys[0]

	// Token Auth instead of Form
	selector = container.NewBorder(f.Message, container.NewHBox(logout, refresh, showspice), nil, nil, list)
	if f.Cfg.TokenAuth {
		f.SetClient("", "", "")
		data.Set(f.GetVms())
		w.SetContent(selector)
	} else {
		// Form
		username := widget.NewEntry()
		username.SetText(f.Cfg.Username)
		password := widget.NewPasswordEntry()
		password.SetText(f.Cfg.Password)
		otp := widget.NewPasswordEntry()
		otp.PlaceHolder = "optional"
		form := &widget.Form{
			Items: []*widget.FormItem{
				{Text: "group", Widget: groups},
				{Text: "username", Widget: username},
				{Text: "password", Widget: password},
				{Text: "otp", Widget: otp},
			},
			CancelText: "Quit",
			OnCancel:   func() { a.Quit() },
			SubmitText: "Login",
			OnSubmit: func() {
				if username.Text == "" || password.Text == "" {
					f.Message.Importance = widget.DangerImportance
					f.Message.SetText("Error: username, and password are required")
					return
				}
				f.SetClient(username.Text, password.Text, otp.Text)
				if f.Message.Text[:7] != "Success" && f.Message.Text[:7] != "Warning" {
					return
				}
				data.Set(f.GetVms())
				w.SetContent(selector)
			},
		}
		username.OnSubmitted = func(s string) { w.Canvas().Focus(password) }
		password.OnSubmitted = func(s string) { form.OnSubmit() }
		otp.OnSubmitted = func(s string) { form.OnSubmit() }
		// Set Login Container
		login = container.NewBorder(container.NewHBox(settings, f.Message), nil, nil, nil, form)
		w.SetContent(login)
	}
	w.ShowAndRun()
}
